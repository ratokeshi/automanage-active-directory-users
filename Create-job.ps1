﻿# Change these three variables to whatever you want
$jobname = "Reset User flag to NOT require password change on login."

$script =  "C:\Users\Public\Scripts\do-not-require-password-change.ps1 -Server localhost"
$repeat = (New-TimeSpan -Minutes 1)


$folderpath = "C:\Users\Public\Scripts" 

#
Write-output "****Begin Script****"
#Todo- check to see if script path exists
Write-Output "$('[{0:MM/dd/yyyy} {0:HH:mm:ss}]' -f (Get-Date)) Begin Folder check."
#$folder = Get-ChildItem $folderpath -Directory -ErrorAction SilentlyContinue
#Write-output $folder 
If (test-path $Folderpath) 
    { Write-host $folder.fullname}
else
{ Write-host "Cannot find folder in path: $folderpath"
    Write-host "Creating missing folder at $('[{0:MM/dd/yyyy} {0:HH:mm:ss}]' -f (Get-Date))."
}
    If(!(Test-path $folderpath))
    {
        New-item -Itemtype Directory -f -Path $folder
        Write-host "Completed folder creation."
    }     
    else 
    { 
        write-host $folder.fullname 
        Write-host "Folder exists."
        
    }
    
Copy-item ".\Scripts\do-not-require-password-change.ps1" -Destination $folderpath



# The script below will run as the specified user (you will be prompted for credentials)
# and is set to be elevated to use the highest privileges.
# In addition, the task will run every 1 minute(s) or however long specified in $repeat.
$scriptblock = [scriptblock]::Create($script)
$trigger = New-JobTrigger -Once -At (Get-Date).Date -RepeatIndefinitely -RepetitionInterval $repeat
$msg = "Enter the username and password that will run the task"; 
$credential = $Host.UI.PromptForCredential("Task username and password",$msg,"$env:userdomain\$env:username",$env:userdomain)

$options = New-ScheduledJobOption -RunElevated -ContinueIfGoingOnBattery -StartIfOnBattery



Register-ScheduledJob -Name $jobname -ScriptBlock $scriptblock -Trigger $trigger -ScheduledJobOption $options -Credential $credential

write-host "****Complete****"