# automanage-active-directory-users

Creates a scheduled job on a Windows server to run Powershell to manage users created by Okta and other deployment methods.

## Objective

1. Copy project locally
1. Run `.\Create-job.ps1` as admin
1. You will get a request to logon as an account you want to run the job as. (wow I hate that grammar)
1. Check in Scheduled Tasks.  This job will appear under Task Scheduler -> Task Scheduler Library -> Microsoft -> Windows -> PowerShell -> ScheduledJobs.
1. To get to the logs/output `%homepath%\AppData\Local\Microsoft\Windows\PowerShell\ScheduledJobs`


## Thanks To and Credit Due

- Chrissy LeMaire Twitter @cl [Great Powershell Reference site by a MSFT MVP](https://www.netnerds.net "Chrissy LeMaire Website") for the [Scheduled job creation](https://blog.netnerds.net/2015/01/create-scheduled-task-or-scheduled-job-to-indefinitely-run-a-powershell-script-every-5-minutes/)

## Todos

- done 2019-08-20 Create script for toggle
- done 2019-08-20 Create password reset toggle script 

```powershell
Import-Module ActiveDirectory
Get-ADGroupMember -Identity "Domain Users" | Set-ADUser -ChangepasswordAtLogon:$False
```

- validate logging
- Event log entry
- xml output cleanup and or shipping
- Create Okta Trigger?
- fix possible file vs folder creation

## Notes
